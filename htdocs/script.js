var counter = 0;

$(document).ready(function() {
    $(document).ready(CreateBall);
    $("#changeColors").click(colorChange);
    $("#smaller").click(function() {
        $(".ball").css("width", "50px");
        $(".ball").css("height", "50px");
    });
    $("#larger").click(function() {
        $(".ball").css("width", "100px");
        $(".ball").css("height", "100px");
    });
        $("#bounce").click(AnimateBall);
});

function CreateBall() {
  $(".ball").css("width", "100px");
  $(".ball").css("height", "100px");
  $(".ball").css("border-radius", "50%");
  $(".ball").css("margin", "0");
  $(".ball").css("top", "200px");
  $(".ball").css("left", "200px");
  $(".ball").css("position", "relative");

  $("#color").ready(colorChange);
}

function colorChange() {
    var hexVals = "0123456789ABCDEF";
      var hexColor = "#";
      for (var q = 0; q < 6; q++)
       hexColor += hexVals[(Math.floor(Math.random() * 16))];
      $('.ball').css('background', hexColor);
}

function AnimateBall() {
    var ballHeight=$(".ball").height();
    var ballWidth=$(".ball").width();
    var checkOne = 1;
    var checkTwo = 2;
    counter++;

    var test = setInterval(function()
    {
        ballHeight = $(".ball").height();
        ballWidth = $(".ball").width();

        if((counter % 2) == 1) {
        $(".ball").css("top", function(i, l) {
            l = parseInt(l);
            if (l+ballHeight+1 > $("#displayarea").height() || l == 0)
                checkOne = checkOne * -1;
            return l+checkOne+"px";
        })

        $(".ball").css("left", function(i, w) {
            w = parseInt(w);
            if (w+ballWidth+1 > $("#displayarea").width() || w == 0)
                checkTwo = checkTwo * -1;
            return w+checkTwo+"px";
        })
      }
      else {
        clearInterval(test);
      }
    }, 20);
}
