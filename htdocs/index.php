<!DOCTYPE html>
<html lang="en">
<head>
<title>Assignment 08 -- Balser, Justin</title>
<link rel="stylesheet" type="text/css" href="styles.css">
<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>

<body>
<script src = "script.js"></script>

  <section>
  <button id="changeColors">Change Colors</button>
  <button id="larger">Larger</button>
  <button id="smaller">Smaller</button>
  <button id="bounce">Bounce</button>

  <div id='displayarea'>
    <div class='ball'></div>
  </div>
</section>

</body>
</html>
